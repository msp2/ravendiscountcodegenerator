% script RavenDiscountCodeGenerator.m
%   Create single-use Raven discount codes for use in Volusion

%---
% Initializations
%---
base = 'RAVENPRO100-'; % couponcode base
n = 50;                % number of couponcodes to produce
k = 10^6;              % number of digits in randomly-generated number

%---
% Generate codes
%---
rng('shuffle')
num = cellstr(num2str(floor(k .* rand(n,1)),'%06.0f'));
code = strcat(base,num);
fn = sprintf('Raven_codes_%s.txt', datestr(now,'yyyymmdd_HHMMSS'));
fnFull = fullfile('..','data', fn);
fid = fopen(fnFull,'wt');
fprintf(fid,'%s\n',code{:});
fclose(fid);
dos(fnFull);
